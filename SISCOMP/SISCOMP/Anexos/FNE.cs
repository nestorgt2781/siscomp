﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SISCOMP.Anexos
{
    public partial class FNE : Form
    {
        public DataTable dt { get; set; }
        public int vida { get; set; }
        public double tasa { get; set; }
        public List<double> LISTA { get; set; }
        public FNE()
        {
            InitializeComponent();
        }

        private void FNE_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = dt;
            textBox1.Text=(Math.Round(VAN(LISTA, tasa, vida),4)).ToString();
            for (double i=0.00001;i<1;i=i+0.00001)
            {
                if (VAN(LISTA,i,vida)<0)
                {
                    double k= (i - 0.00001)*100;
                    if (k>tasa)
                    {
                        label4.Text=("la TIR mayor que TMAR se acepta el proyecto");
                    }
                    else
                    {
                        label4.Text=("No se acepta el proyecto");
                    }
                    textBox2.Text=(Math.Round(k, 4).ToString()+"%");
                    i = 1;
                }
            }
            MessageBox.Show(VAN(LISTA, 0.01, vida).ToString() );
        }
        public double VAN(List<double> datos,double tasa,int vida)
        {
            double vpn = datos.ElementAt(0);
            
            for (int i=1;i<=vida;i++)
            {
                vpn=vpn+datos.ElementAt(i)/(Math.Pow((1+tasa),i));
            }
            return vpn;
        }
    }
}
