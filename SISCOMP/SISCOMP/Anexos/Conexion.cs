﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace SISCOMP.Anexos
{
    public class Conexion
    {
        public SqlConnection connec = new SqlConnection();
        public Conexion()
        {
            try
            {
                connec = new SqlConnection("Server=SISTEMAS13\\SQLSERVER2019;Database=siscomp;Integrated Security=true");
                connec.Open();
            }catch
            {
            }

        }
        public DataTable ListarEntidad(string vista)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText="select * from "+vista;
            cmd.Connection = connec;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            connec.Close();
            return dt;
        }
    }
}
